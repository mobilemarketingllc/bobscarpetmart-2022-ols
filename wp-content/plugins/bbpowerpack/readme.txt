=== PowerPack for Beaver Builder ===
Contributors: ideaboxcreations, ibachal, puneetsahalot
Tags: beaver builder, beaver builder free, beaver addons, beaver builder addon, beaver builder add ons, beaver builder lite, beaver builder modules, beaver builder addons, beaver builder extensions, beaver addon, beaver builder plugin, beaver builder wordpress
Requires at least: 4.6
Requires PHP: 5.6
Tested up to: 6.7
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

PowerPack extends Beaver Builder with 90+ custom, creative, unique modules and 250+ templates to speed up your web design and development process.