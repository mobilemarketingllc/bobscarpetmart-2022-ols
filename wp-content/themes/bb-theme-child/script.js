// header flyer choose location js start here 
jQuery(document).on('click', '.makemystore', function() {
  
    var mystore = jQuery(this).attr("data-store-id");  

    //alert(mystore);

    jQuery.cookie("preferred_store", null, { path: '/' });   
    jQuery.cookie("preferred_storename", null, { path: '/' });
    jQuery.cookie("preferred_address", null, { path: '/' });
    jQuery.cookie("preferred_phone", null, { path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=make_my_store&store_id=' + jQuery(this).attr("data-store-id"),
        dataType: 'JSON',

        success: function(data) {          

            var posts = JSON.parse(JSON.stringify(data));
            var store_name = posts.store_title;
            var store_add = posts.store_address;        
            var store_phone = posts.header_phone;  
           
            jQuery(".header_location_name").html(store_name);
            jQuery(".head_phone").text(store_phone);
            jQuery('a.head_phone').attr("href", "tel:" + store_phone);  
            
            jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });           
            jQuery.cookie("preferred_storename", store_name, { expires: 1, path: '/' });
            jQuery.cookie("preferred_address", store_add, { expires: 1, path: '/' });
            jQuery.cookie("preferred_phone", store_phone, { expires: 1, path: '/' });
         
            	
        }
    });

});



// flyer close function 
function closeNav() {
    let storeLocation = document.getElementById("storeLocation");
    storeLocation.style.right = "-100%";
    storeLocation.classList.remove("ForOverlay");
}

// flyer event assign function
function addFlyerEvent() {
   jQuery('.locationWrapFlyer .dropIcon').click(function() {
        jQuery('#storeLocation').toggle();
    })
}


jQuery(document).ready(function() {

    jQuery("#wpsl-search-input").attr("placeholder", "Enter Your Zip Code");   
   
    var preferred_address = jQuery.cookie("preferred_address");
    var preferred_store = jQuery.cookie("preferred_store");
    var preferred_storename = jQuery.cookie("preferred_storename");     
      
    
    jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=get_storelisting',
       dataType: 'JSON',
       success: function(response) {
          
               var posts = JSON.parse(JSON.stringify(response));
               var header_data = posts.header;
               var list_data = posts.list;
               var header_phone = posts.phone;
               var locname = posts.header;          
               var locid = posts.store_name;
               var loc_address = posts.address; 

              // console.log(posts.list);
  
               var mystore_loc =  jQuery.cookie("preferred_storename");      
               
               if(preferred_store == null){
  
                jQuery.cookie("preferred_store", posts.store_id, { expires: 1, path: '/' });               
                jQuery.cookie("preferred_storename", locname, { expires: 1, path: '/' });
                jQuery.cookie("preferred_address", loc_address, { expires: 1, path: '/' });    
                jQuery.cookie("preferred_phone", header_phone, { expires: 1, path: '/' });                      
                
                
               }         
               
               jQuery(".header_store .header_location_name").html(header_data);      
               jQuery(".head_phone").text(header_phone);     
               jQuery('a.head_phone').attr("href", "tel:" + header_phone);  
               jQuery("#storeLocation .content").html(list_data);
               jQuery(".populate-store select").val(header_data);
            
             
       }
   });
  
  });



  jQuery(document).ready(function() {

    var mystore_loc =  jQuery.cookie("preferred_storename");
    //console.log(mystore_loc);
    jQuery(".populate-store select").val(mystore_loc);

});

 jQuery( document ).ready(function() {
     
    jQuery('.searchIcon .fl-icon').click(function(){
        jQuery('.searchModule').slideToggle();
    });    
   

    jQuery(document).mouseup(function(e) {
        var container = jQuery(".searchModule, .searchIcon");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0 && jQuery('.searchModule').css('display') !=='none') 
        {
            jQuery('.searchModule').slideToggle();
        }
    });
});