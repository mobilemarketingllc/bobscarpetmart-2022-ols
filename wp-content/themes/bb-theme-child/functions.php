<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions

add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);   
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);    
});

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


//add_filter( 'wpsl_admin_marker_dir', 'custom_admin_marker_dir' );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

function custom_admin_marker_dir() {

    $admin_marker_dir = get_stylesheet_directory() . '/wpsl-markers/';
    
    return $admin_marker_dir;
}

add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}


//the start marker will not show up anymore.
add_filter( 'wpsl_js_settings', 'custom_js_settings' );

function custom_js_settings( $settings ) {

    $settings['startMarker'] = '';

    return $settings;
}


//Custom filter for Store listing on find retailer page

add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

    global $wpsl, $wpsl_settings;
    
    $listing_template = '<li data-store-id="<%= id %>" id="<%= id %>">' . "\r\n";
    $listing_template .= "\t\t" . '<div class="wpsl-store-location">' . "\r\n";
    $listing_template .= "\t\t\t\t" . custom_store_header_template( 'listing' ) . "\r\n"; // Check which header format we use
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address2 %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n"; // Use the correct address format

    if ( !$wpsl_settings['hide_country'] ) {
        $listing_template .= "\t\t\t\t" . '<span class="wpsl-country"><%= country %></span>' . "\r\n";
    }

    $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    
    
    if ( $wpsl_settings['show_contact_details'] ) {
        $listing_template .= "\t\t\t" . '<p class="wpsl-contact-details">' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><%= formatPhoneNumber( phone ) %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( fax ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . '</strong>: <%= fax %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( email ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'email_label', __( 'Email', 'wpsl' ) ) ) . '</strong>: <%= email %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
       // $listing_template .= "\t\t\t" . '<% if ( site_url ) { %>' . "\r\n";
      //  $listing_template .= "\t\t\t" . '<span><a href="https://<%= site_url %>" target="_blank" class="site_loc_url"><%= site_url %></a></span>' . "\r\n";
       // $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    }

    $listing_template .= "\t\t" . '<div class="Coupon-wrapper">' . "\r\n";

     $listing_template .= "\t\t\t" . '<div class="makemystore_wrap"><a href="javascript:void(0)"
     class="makemystore fl-button" data-store-id="<%= id %>"> MAKE MY STORE</a></div>' . "\r\n";

     $listing_template .= "\t\t\t" . '<div class="get_details "><a href="<%= permalink %>"
     class="fl-button"> GET DETAILS</a></div>' . "\r\n";

   //  $listing_template .= "\t\t\t" . '<div class="visit_store "><a target="_blank" href="https://<%= site_url %>"
  //   class="fl-button"> VISIT STORE</a></div>' . "\r\n";

    $listing_template .= "\t\t" . '</div>' . "\r\n";

    $listing_template .= "\t\t\t" . wpsl_more_info_template() . "\r\n"; // Check if we need to show the 'More Info' link and info
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t\t" . '<div class="wpsl-direction-wrap">' . "\r\n";
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t" . '</li>';

    return $listing_template;
}

add_filter( 'wpsl_store_header_template', 'custom_store_header_template' );

function custom_store_header_template() {

    global $wpsl, $wpsl_settings;
    
    $header_template = '<div class="location_title_wrapper"><div class="your_store"></div><h3><a href="<%= permalink %>"><%= store.toUpperCase() %></a></h3>';
    $header_template .= "\t\t\t" . '<span class="distance_unit"><%= distance %> MILES</span>' . "\r\n";
    $header_template .= '</div>';
    
    return $header_template;
}

add_filter( 'wpsl_store_header_template', 'custom_info_header_template' );

function custom_info_header_template() {

     global $wpsl, $wpsl_settings;
    
    $header_template = '<div class="location_title_wrapper"><h3><a href="<%= permalink %>"><%= store.toUpperCase() %></a></h3></div>';
    
    return $header_template;
}


add_filter( 'wpsl_info_window_template', 'custom_info_window_template' );
//add_filter( 'wpsl_cpt_info_window_template', 'custom_info_window_template' );

function custom_info_window_template() {

    global $wpsl_settings, $wpsl;
   
    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window custominfo_wrapper">' . "\r\n";
    $info_window_template .= "\t\t\t" . '<div class="store_thumb"><img alt="store_thumb" src="/wp-content/uploads/2021/11/bobs-carpet-and-flooring.png" /></div>' . "\r\n"; 
    
    $info_window_template .= "\t\t\t" . '<div class="info_wrapper">' . "\r\n";
    $header_template = '<div class="location_title_wrapper"><h3><a href="#"><%= store.toUpperCase() %></a></h3></div>'; 

    $info_window_template .= "\t\t\t\t" . custom_info_header_template( ) . "\r\n"; // Check which header format we use
   
    $info_window_template .= "\t\t\t" . '<div class="infomap_div"><i class="ua-icon ua-icon-location-pin" aria-hidden="true"></i><p><%= address %> '. wpsl_address_format_placeholders() .'</p>' . "\r\n";
   // $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
  //  $info_window_template .= "\t\t\t" . '<span class="infomap_add"><p><%= address2 %></p></span>' . "\r\n";
  //  $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
  // $info_window_template .= "\t\t\t" . '' . wpsl_address_format_placeholders() . '</p>' . "\r\n";
   $info_window_template .= "\t" . '</div>' . "\r\n";

    $info_window_template .= "\t\t" . '<span class="store_msg">TAMPA BAYS BEST FLOORING STORE SINCE 1969</span>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
      
    $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
    
    return $info_window_template;
}


add_filter( 'wpsl_cpt_info_window_template', 'custom_cpt_info_window_template' );

function custom_cpt_info_window_template() {
    
    global $wpsl_settings, $wpsl;
   
    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window custominfo_wrapper cpt_info_window">' . "\r\n";
    $info_window_template .= "\t\t\t" . '<div class="store_thumb"><img alt="store_thumb" src="/wp-content/uploads/2021/11/bobs-carpet-and-flooring.png" /></div>' . "\r\n"; 
    
    $info_window_template .= "\t\t\t" . '<div class="info_wrapper">' . "\r\n";
    $info_window_template .= "\t\t\t" . '<div class="location_title_wrapper"><h3><%= store.toUpperCase() %></h3></div>'. "\r\n";

    //$info_window_template .= "\t\t\t\t" . custom_info_header_template( ) . "\r\n"; // Check which header format we use
   
    $info_window_template .= "\t\t\t" . '<div class="infomap_div"><i class="ua-icon ua-icon-location-pin" aria-hidden="true"></i><p><%= address %> '. wpsl_address_format_placeholders() .'</p>' . "\r\n";
   // $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
  //  $info_window_template .= "\t\t\t" . '<span class="infomap_add"><p><%= address2 %></p></span>' . "\r\n";
  //  $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
  // $info_window_template .= "\t\t\t" . '' . wpsl_address_format_placeholders() . '</p>' . "\r\n";
   $info_window_template .= "\t" . '</div>' . "\r\n";

    $info_window_template .= "\t\t" . '<span class="store_msg">TAMPA BAYS BEST FLOORING STORE SINCE 1969</span>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
      
   // $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
    
    return $info_window_template;
}


add_filter( 'wpsl_store_data', 'custom_store_data_sort' );

function custom_store_data_sort( $stores ) {

    // Create the array that holds the featured locations
    $featured_list = array();

    // Loop over the collected location list
    foreach ( $stores as $k => $store ) {

        //write_log('cookie-'.$_COOKIE['preferred_store']);
       // write_log('store id-'.$store['id']);

        // Check if the location is a featured one
        if ( isset( $_COOKIE['preferred_store'] ) && $_COOKIE['preferred_store'] == $store['id'] ) {            

            // Move the featured locations to a new array
            $featured_list[] = $store;

            // Remove the featured location from the existing $stores array
            unset( $stores[$k] );
        }
    }

    /**
     * Merge the list of premium locations with the existing list.
     * This will make sure the premium location show up before the normal locations.
     */
    $results = array_merge( $featured_list, $stores );

    return $results;
}

add_filter( 'wpsl_thumb_size', 'custom_thumb_size' );

function custom_thumb_size() {
    
    $size = array( 100, 100 );
    
    return $size;
}

//Location sync

add_action( 'init', 'register_bbsync_cron_delete_event');

// Function which will register the event
function register_bbsync_cron_delete_event() {
	if( !wp_next_scheduled( 'bb_location_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'daily', 'bb_location_sync' );
    }
}
   

register_activation_hook(__FILE__, 'bb_sync_hook_activation');

/**  Function for activate cron job  **/
function bb_sync_hook_activation() {
    if (! wp_next_scheduled ( 'bb_location_sync' )) {
	    wp_schedule_event(time(), 'daily', 'bb_location_sync');
    }
}

add_action( 'bb_location_sync', 'bb_location_sync_function' );

function bb_location_sync_function() {

    global $wpdb;

    if ( ! function_exists( 'post_exists' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/post.php' );
    }

    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){

        //API Call for getting website INFO
        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

     
        for($i=0;$i<count($website['result']['locations']);$i++){



            if($website['result']['locations'][$i]['type'] == 'store'){     
                
               // write_log( $website['result']['locations'][$i]['name'].'---'.$website['result']['locations'][$i]['city']);
                $location = $apiObj->call(BASEURL.$website['result']['locations'][$i]['clientCode'],"GET",$inputs,$headers);           

              

                $location_url = $website['result']['locations'][$i]['url'];

                $location_rugcode = $location['result']['rugAffiliateCode'];   

                $location_name = $website['result']['locations'][$i]['name'];            

                $found_post = post_exists($location_name,'','','wpsl_stores');

                $store_hours = array();

                              $store_hours = array(
                                'monday'=> array(str_replace('-',',',$website['result']['locations'][$i]['monday'])),
                                'tuesday'=>array(str_replace('-',',',$website['result']['locations'][$i]['tuesday'])),
                                'wednesday'=>array(str_replace('-',',',$website['result']['locations'][$i]['wednesday'])),
                               'thursday'=>array(str_replace('-',',',$website['result']['locations'][$i]['thursday'])),
                                'friday'=>array(str_replace('-',',',$website['result']['locations'][$i]['friday'])),
                                'saturday'=>array(str_replace('-',',',$website['result']['locations'][$i]['saturday'])),
                                'sunday'=>array(str_replace('-',',',$website['result']['locations'][$i]['sunday']))
                                             
                              );

                $store_hours_json = serialize($store_hours);
             

                     if( $found_post == 0 ){

                            $array = array(
                                'post_title' => $location_name,
                                'post_type' => 'wpsl_stores',
                                'post_content'  => "",
                                'post_status'   => 'publish',
                                'post_author'   => 0,
                            );
                            $post_id = wp_insert_post( $array );                        
                            
                            update_post_meta($post_id, 'wpsl_address', $website['result']['locations'][$i]['address']); 
                            update_post_meta($post_id, 'wpsl_city', $website['result']['locations'][$i]['city']); 
                            update_post_meta($post_id, 'wpsl_state', $website['result']['locations'][$i]['state']); 
                            update_post_meta($post_id, 'wpsl_country', $website['result']['locations'][$i]['country']); 
                            update_post_meta($post_id, 'wpsl_zip', $website['result']['locations'][$i]['postalCode']); 
                            update_post_meta($post_id, 'wpsl_store_clientcode', $website['result']['locations'][$i]['clientCode']); 
                            update_post_meta($post_id, 'wpsl_rugshop_code', $location_rugcode); 

                            if($website['result']['locations'][$i]['forwardingPhone']==''){

                              update_post_meta($post_id, 'wpsl_phone', $website['result']['locations'][$i]['phone']);  
                              
                            }else{

                              update_post_meta($post_id, 'wpsl_phone', $website['result']['locations'][$i]['forwardingPhone']);  
                            }
                                                               
                            update_post_meta($post_id, 'wpsl_lat', $website['result']['locations'][$i]['lat']); 
                            update_post_meta($post_id, 'wpsl_lng', $website['result']['locations'][$i]['lng']); 
                            update_post_meta($post_id, 'wpsl_store_shortname', $location_name); 
                            update_post_meta($post_id, 'wpsl_site_url', $location_url); 
                            update_post_meta($post_id, 'wpsl_hours', $store_hours_json);
                            
                            
                    }else{

                              update_post_meta($found_post, 'wpsl_address', $website['result']['locations'][$i]['address']); 
                              update_post_meta($found_post, 'wpsl_city', $website['result']['locations'][$i]['city']); 
                              update_post_meta($found_post, 'wpsl_state', $website['result']['locations'][$i]['state']); 
                              update_post_meta($found_post, 'wpsl_country', $website['result']['locations'][$i]['country']); 
                              update_post_meta($found_post, 'wpsl_zip', $website['result']['locations'][$i]['postalCode']); 
                              update_post_meta($found_post, 'wpsl_store_clientcode', $website['result']['locations'][$i]['clientCode']); 
                              update_post_meta($found_post, 'wpsl_rugshop_code', $location_rugcode); 
                              if($website['result']['locations'][$i]['forwardingPhone']==''){

                              update_post_meta($found_post, 'wpsl_phone', $website['result']['locations'][$i]['phone']);  
                              
                              }else{

                              update_post_meta($found_post, 'wpsl_phone', $website['result']['locations'][$i]['forwardingPhone']);  
                              }
                                                              
                               update_post_meta($found_post, 'wpsl_lat', $website['result']['locations'][$i]['lat']); 
                               update_post_meta($found_post, 'wpsl_lng', $website['result']['locations'][$i]['lng']); 
                               update_post_meta($found_post, 'wpsl_store_shortname', "Big Bob's Flooring - ".$state); 
                               update_post_meta($found_post, 'wpsl_store_shortname', $location_name); 
                               update_post_meta($found_post, 'wpsl_site_url', $location_url);
                               update_post_meta($found_post, 'wpsl_hours', $store_hours_json);
                              

                    }

            }
        
        }

    }    

}

add_filter( 'gform_pre_render_4', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_4', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_4', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_4', 'populate_product_location_form' );

add_filter( 'gform_pre_render_6', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_6', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_6', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_6', 'populate_product_location_form' );

add_filter( 'gform_pre_render_10', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_10', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_10', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_10', 'populate_product_location_form' );

add_filter( 'gform_pre_render_12', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_12', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_12', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_12', 'populate_product_location_form' );

add_filter( 'gform_pre_render_14', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_14', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_14', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_14', 'populate_product_location_form' );


function populate_product_location_form( $form ) {

foreach ( $form['fields'] as &$field ) {

    // Only populate field ID 12
    if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
        continue;
    }      	

        $args = array(
            'post_type'      => 'wpsl_stores',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );										
      
         $locations =  get_posts( $args );

         $choices = array(); 

         foreach($locations as $location) {
            

             // $title =  get_post_meta( $location->ID, 'city', true ).', '.get_post_meta( $location->ID, 'state', true );

             $title = get_the_title($location->ID);
              
                   $choices[] = array( 'text' => $title, 'value' => $title );

          }
          wp_reset_postdata();

         // write_log($choices);

         // Set placeholder text for dropdown
         $field->placeholder = '-- Choose Location --';

         // Set choices from array of ACF values
         $field->choices = $choices;
    
}
return $form;
}

//make_my_store FUnctionality

add_action( 'wp_ajax_nopriv_make_my_store', 'make_my_store' );
add_action( 'wp_ajax_make_my_store', 'make_my_store' );

function make_my_store() {
    
     $data = array(); 
     $store_title = get_the_title( $_POST['store_id']);      
     $store_address = get_post_meta( $_POST['store_id'], 'wpsl_address',true ).' '.get_post_meta($_POST['store_id'],'wpsl_city',true).', '.get_post_meta($_POST['store_id'],'wpsl_state',true).' '.get_post_meta($_POST['store_id'],'wpsl_zip',true);
     $data['store_title']= $store_title;
     $data['header_phone'] = formatPhoneNumber(get_post_meta( $_POST['store_id'], 'wpsl_phone',true ));
     $data['store_address']= $store_address;     
     
     $store_hours_meta = get_post_meta( $_POST['store_id'], 'wpsl_hours',true );      
     $store_hours = maybe_unserialize( $store_hours_meta );
     $currentday = date("l");
   

     foreach ($store_hours as $mdaKey => $mdaData) {

         if(strtolower($currentday) == $mdaKey){
          

               // write_log($store_title.'----'.$mdaKey . ": " . $mdaData[0]);
                $store_hour = explode(",",$mdaData[0]);
               

                if($store_hour[0] =='Closed' || $store_hour[0] == 'CLOSED'){

                    $data['store_status'] = 'CLOSED';
                    $data['store_hour'] = '';

                }else if($store_hour[0] =='Appointment Only' || $store_hour[0] == 'Appointment'){

                    $data['store_status'] = 'Appointment Only';
                    $data['store_hour'] = '';
                
               }else{

                    $data['store_status'] = 'Open';
                    $data['store_hour'] = $store_hour[0].' - '.$store_hour[1];
                } 

         }      
    }

     echo json_encode($data);    	
    
     wp_die();
}

function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    
    if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
  }

// Custom function for lat and long

function get_storelisting() {
  
  global $wpdb;
  $content="";
  $data = array();
  
  $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';

  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];
      if(isset($_COOKIE['preferred_store'])){

          $store_location = $_COOKIE['preferred_store'];
      }else{

          $store_location = '';
         
        }      
      
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
        ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
        cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
        sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
        INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
        INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'  
        WHERE posts.post_type = 'wpsl_stores' 
        AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

           

      $storeposts = $wpdb->get_results($sql);
      $storeposts_array = json_decode(json_encode($storeposts), true);      

     
      if($store_location ==''){
              
          $store_location = $storeposts_array['0']['ID'];
          $store_distance = round($storeposts_array['0']['distance'],1);
          
      }else{

      
          $key = array_search($store_location, array_column($storeposts_array, 'ID'));
          $store_distance = round($storeposts_array[$key]['distance'],1);           

      }
      
    
      $content = get_the_title($store_location); 
      $phone = get_post_meta( $store_location, 'wpsl_phone', true );


  foreach ( $storeposts as $post ) {


    $store_hours_meta = get_post_meta( $post->ID, 'wpsl_hours',true );      
    $store_hours = maybe_unserialize( $store_hours_meta );
    $currentday = date("l");
  

    foreach ($store_hours as $mdaKey => $mdaData) {

        if(strtolower($currentday) == $mdaKey){
         

               //write_log($store_title.'----'.$mdaKey . ": " . $mdaData[0]);
               $store_hourd = explode(",",$mdaData[0]);
               $store_hour = explode(" – ",$store_hourd[0]);
               write_log($store_hour);

               if($store_hour[0] =='Closed' || $store_hour[0] == 'CLOSED'){

                   $data['store_status'] = 'CLOSED';
                   $data['store_hour'] = '';

               }else if($store_hour[0] =='Appointment Only' || $store_hour[0] == 'Appointment'){

                   $data['store_status'] = 'Appointment Only';
                   $data['store_hour'] = '';
               
              }else{

                   $data['store_status'] = 'Open';
                   $data['store_hour'] = str_replace("", "-", $store_hour[1]);
               } 

        }      
   }


                    $content_list .= '<div class=" store_wrapper" id ="'.$post->ID.'">
                
                    <h5 class="title-prefix">'.get_the_title($post->ID).' - <b>'.round($post->distance,1).' MI</b></h5>
                    <h5 class="store-add"> '.get_post_meta( $post->ID, 'wpsl_address', true ).'<br />'.get_post_meta( $post->ID, 'wpsl_city', true ).', '.get_post_meta( $post->ID, 'wpsl_state', true ).' '.get_post_meta( $post->ID, 'wpsl_zip', true ).'</h5>';
             if ( $post->ID =='1875295') {     
	 				 $content_list .='<p class="store-phone">Open until 7:00 pm</p>';
		   	}  else 
				 {  
				 $content_list .='<p class="store-phone">Open until '.$post->ID.$data['store_hour'].'</p>';
				  }  
                    
      
if (!empty(get_post_meta( $post->ID, 'wpsl_phone', true ))) { 
                    $content_list .='<p class="store-phone"><a href="tel:'.get_post_meta( $post->ID, 'wpsl_phone', true ).'">'.get_post_meta( $post->ID, 'wpsl_phone', true ).'</a> </p>'; 
 }  
                    $content_list .= '<a href="javascript:void(0)" data-store-id="'.$post->ID.'" data-distance="8876.4" data-storename="'.get_the_title($post->ID).'" data-phone="'.get_post_meta( $post->ID, 'wpsl_phone', true ).'" target="_self" class="store-cta-link makemystore">Choose This Location</a>';

                    $content_list .= '<a href="'.get_permalink($post->ID).'" target="_self" class="store-cta-link view_location"> View Location</a>';
                   
                    $content_list .='</div>'; 
  } 
  

 

  $data['header']= $content;
  $data['phone']= $phone;
  $data['list']= $content_list;
  $data['store_id']=  $store_location;

  echo json_encode($data);
      wp_die();
}


add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );


//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'carpeting' )) {
       
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/carpet/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'hardwood_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood-flooring/',
            'text' => 'Hardwood Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/hardwood-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate-flooring/',
            'text' => 'Laminate Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/laminate-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/luxury-vinyl-flooring/',
            'text' => ' Luxury Vinyl / Waterproof',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/luxury-vinyl-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    if (is_singular( 'tile_catalog' )) {

        
        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/tile/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    
    return $links;
}




function check_bob_404($url) {
    $headers=get_headers($url, 1);
    if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
 }

 wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );

 if (!wp_next_scheduled('404_bobsmart_redirection_301_log_cronjob')) {
    
    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time() +  17800, 'daily', '404_bobsmart_redirection_301_log_cronjob');
}

add_action( '404_bobsmart_redirection_301_log_cronjob', 'bobsmart_404_redirect_hook' ); 
 
  // custom 301 redirects from  404 logs table
  function bobsmart_404_redirect_hook(){
     global $wpdb;    
     write_log('in function');
 
     $table_redirect = $wpdb->prefix.'redirection_items';
     $table_name = $wpdb->prefix . "redirection_404";
     $table_group = $wpdb->prefix.'redirection_groups';
 
      $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
      $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
      $redirect_group =  $datum[0]->id;  
 
     if ($data_404)
     {      
       foreach ($data_404 as $row_404) 
        {            
 
         if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false ) {
             write_log($row_404->url);      
             
            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
 
             $url_404 = home_url().''.$row_404->url;
             $headers_res = check_bob_404($url_404);
 
            if($checkalready == '' && $headers_res == 'false'){  
 
            
             $destination_url_carpet = '/carpet/products/';
             $match_url = rtrim($row_404->url, '/');
 
             $data = array("url" => $row_404->url,
             "match_url" => $match_url,
             "match_data" => "",
             "action_code" => "301",
             "action_type" => "url",
             "action_data" => $destination_url_carpet,
             "match_type" => "url",
             "title" => "",
             "regex" => "true",
             "group_id" => $redirect_group,
             "position" => "1",
             "last_access" => current_time( 'mysql' ),
             "status" => "enabled");
 
             $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');
 
             $wpdb->insert($table_redirect,$data,$format);
 
             $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
             write_log( 'carpet 301 added ');
          }else{
 
             $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
 
          }
 
         }
         else if (strpos($row_404->url,'hardwood-flooring') !== false && strpos($row_404->url,'products') !== false ) {
 
             write_log($row_404->url);
 
             $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
             $url_404 = home_url().''.$row_404->url;
             $headers_res = check_bob_404($url_404);
 
            if($checkalready == '' && $headers_res == 'false'){  
 
             
             $destination_url_hardwood = '/hardwood-flooring/products/';
             $match_url = rtrim($row_404->url, '/');
 
             $data = array("url" => $row_404->url,
             "match_url" => $match_url,
             "match_data" => "",
             "action_code" => "301",
             "action_type" => "url",
             "action_data" => $destination_url_hardwood,
             "match_type" => "url",
             "title" => "",
             "regex" => "true",
             "group_id" => $redirect_group,
             "position" => "1",
             "last_access" => current_time( 'mysql' ),
             "status" => "enabled");
 
             $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');
 
             $wpdb->insert($table_redirect,$data,$format);
             $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
            }else{
 
             $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
            }
 
             write_log( 'hardwood 301 added ');
 
         }
         else if (strpos($row_404->url,'laminate-flooring') !== false && strpos($row_404->url,'products') !== false ) {
 
             write_log($row_404->url); 
 
             $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
 
             $url_404 = home_url().''.$row_404->url;
             $headers_res = check_bob_404($url_404);
 
            if($checkalready == '' && $headers_res == 'false'){  
 
             
             $destination_url_laminate = '/laminate-flooring/products/';
             $match_url = rtrim($row_404->url, '/');
 
             $data = array("url" =>$row_404->url,
             "match_url" => $match_url,
             "match_data" => "",
             "action_code" => "301",
             "action_type" => "url",
             "action_data" => $destination_url_laminate,
             "match_type" => "url",
             "title" => "",
             "regex" => "true",
             "group_id" => $redirect_group,
             "position" => "1",
             "last_access" => current_time( 'mysql' ),
             "status" => "enabled");
 
             $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');
 
             $wpdb->insert($table_redirect,$data,$format);
             $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
             }else{
 
                 $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
             }
 
             write_log( 'laminate 301 added ');
 
 
         }
         else if ( strpos($row_404->url,'luxury-vinyl-flooring') !== false  && strpos($row_404->url,'products') !== false ) {
 
             write_log($row_404->url);  
 
             $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
 
             $url_404 = home_url().''.$row_404->url;
             $headers_res = check_bob_404($url_404);
 
            if($checkalready == '' && $headers_res == 'false'){  
 
             
           
                 $destination_url_lvt = '/luxury-vinyl-flooring/products/';
            
             
             $match_url = rtrim($row_404->url, '/');
 
             $data = array("url" =>$row_404->url,
             "match_url" => $match_url,
             "match_data" => "",
             "action_code" => "301",
             "action_type" => "url",
             "action_data" => $destination_url_lvt,
             "match_type" => "url",
             "title" => "",
             "regex" => "true",
             "group_id" => $redirect_group,
             "position" => "1",
             "last_access" => current_time( 'mysql' ),
             "status" => "enabled");
 
             $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');
 
             $wpdb->insert($table_redirect,$data,$format);
             $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
             }else{
 
                 $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
             }
 
             write_log( 'luxury-vinyl 301 added ');
         }
         else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false ) {
 
             write_log($row_404->url); 
 
             $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
 
             $url_404 = home_url().''.$row_404->url;
             $headers_res = check_bob_404($url_404);
 
            if($checkalready == '' && $headers_res == 'false'){  
 
             $destination_url_tile = '/tile/products/';
             $match_url = rtrim($row_404->url, '/');
 
             $data = array("url" => $row_404->url,
             "match_url" => $match_url,
             "match_data" => "",
             "action_code" => "301",
             "action_type" => "url",
             "action_data" => $destination_url_tile,
             "match_type" => "url",
             "title" => "",
             "regex" => "true",
             "group_id" => $redirect_group,
             "position" => "1",
             "last_access" => current_time( 'mysql' ),
             "status" => "enabled");
 
             $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');
 
             $wpdb->insert($table_redirect,$data,$format);
             $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
             }else{
 
                 $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
 
             }
 
             write_log( 'tile 301 added ');
         }
 
        }  
     }
 
  }
function my_cache_lifetime( $seconds ) {
  return 5184000; // 10 day. Default: 3600 (one hour)
}
add_filter( 'facetwp_cache_lifetime', 'my_cache_lifetime' );


//sigle store locator post type template

add_filter( 'single_template', 'childtheme_get_custom_post_type_template' );

function childtheme_get_custom_post_type_template($single_template) {
    global $post;

if ($post->post_type != 'post'){
      
    if($post->post_type === 'wpsl_stores') {

        $single_template = get_stylesheet_directory().'/product-listing-templates/single-'.$post->post_type.'.php';        

    }else{

        $single_template = get_stylesheet_directory().'/product-listing-templates/single-'.$post->post_type.'.php';
    }
}
    return $single_template;
}


//Cron job for sync catalog for all mohawk categories

if (! wp_next_scheduled ( 'sync_mohawk_catalog_all_categories')) {
  
    wp_schedule_event( strtotime("last Sunday of ".date('M')." ".date('Y').""), 'monthly', 'sync_mohawk_catalog_all_categories');
}


//add_action( 'sync_mohawk_catalog_all_categories', 'mohawk_product_sync', 10, 2 );

function mohawk_product_sync(){

    write_log("Only mohawk Catalog sync is running"); 

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';  
    
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	
    $product_json =  json_decode(get_option('product_json'));     

    $brandmapping = array(
        "hardwood"=>"hardwood_catalog",
        "laminate"=>"laminate_catalog",
        "lvt"=>"luxury_vinyl_tile",
        "tile"=>"tile_catalog"      
    );

    foreach($brandmapping as $key => $value){               
       
            $productcat_array = getArrayFiltered('productType',$key,$product_json);               

            
            foreach ($productcat_array as $procat){

                if($procat->manufacturer == "Mohawk"){

                    $permfile = $upload_dir.'/'.$value.'_'.$procat->manufacturer.'.json';
                    $res = SOURCEURL.get_option('SITE_CODE').'/www/'.$key.'/'.$procat->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
                    $tmpfile = download_url( $res, $timeout = 900 );

                        if(is_file($tmpfile)){
                            copy( $tmpfile, $permfile );
                            unlink( $tmpfile ); 
                        } 

                          $sql_delete = "DELETE p, pm FROM $table_posts p INNER JOIN $table_meta pm ON pm.post_id = p.ID  WHERE p.post_type = '$value' AND pm.meta_key = 'manufacturer' AND  pm.meta_value = 'Mohawk'" ;						
                          write_log($sql_delete); 
                          $delete_endpoint = $wpdb->get_results($sql_delete);
                          write_log("mohawk product deleted"); 
                          //exit;

                        write_log('auto_sync - Shaw catalog - '.$key.'-'.$procat->manufacturer);
                       $obj = new Example_Background_Processing();
                       $obj->handle_all($value, $procat->manufacturer);

                        write_log('Sync Completed - '.$procat->manufacturer);

                  }
                    
                }
                
                   write_log('Sync Completed for all  '.$key.' brand');
        }                
}


if( !wp_next_scheduled( 'mm_retailer_mysql_bin_log_job' ) ) {
    // schedule an event
    wp_schedule_event( time(), 'twicedaily', 'mm_retailer_mysql_bin_log_job' );
}

add_action( 'mm_retailer_mysql_bin_log_job', 'mm_retailer_mysql_bin_log_job_function' );

function mm_retailer_mysql_bin_log_job_function() {

global $wpdb;
$yesterday = date('Y-m-d',strtotime("-1 days"));
$sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
$delete_endpoint = $wpdb->get_results($sql_delete);
write_log($sql_delete);

}

if( !wp_next_scheduled( 'orphan_mysql_bin_log_job' ) ) {
    // schedule an event
    wp_schedule_event( time(), 'bimonthly', 'orphan_mysql_bin_log_job' );
}

add_action( 'orphan_mysql_bin_log_job', 'orphan_mysql_bin_log_job_function' );

function orphan_mysql_bin_log_job_function() {

global $wpdb;
$sql_orphan = "DELETE wp_postmeta FROM wp_postmeta
LEFT JOIN wp_posts ON wp_posts.ID = wp_postmeta.post_id
WHERE wp_posts.ID IS NULL" ;						
$delete_orphan = $wpdb->get_results($sql_orphan);

}