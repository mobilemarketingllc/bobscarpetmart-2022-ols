<?php get_header(); ?>

<?php
	global $post;

	$queried_object = get_queried_object();

	$post = get_post( $queried_object->ID );
                    setup_postdata( $post );
                    the_content();
                    wp_reset_postdata( $post );

		$store_shortname       = get_post_meta( $queried_object->ID, 'wpsl_store_shortname', true );
		$address       = get_post_meta( $queried_object->ID, 'wpsl_address', true );
		$city          = get_post_meta( $queried_object->ID, 'wpsl_city', true );
		$zip          = get_post_meta( $queried_object->ID, 'wpsl_zip', true );
		$state          = get_post_meta( $queried_object->ID, 'wpsl_state', true );
		$phone       = get_post_meta( $queried_object->ID, 'wpsl_phone', true );
		$site_url       = get_post_meta( $queried_object->ID, 'wpsl_site_url', true );
		$url       = get_post_meta( $queried_object->ID, 'wpsl_url', true );
		$country       = get_post_meta( $queried_object->ID, 'wpsl_country', true );
		$destination   = $address . ', ' . $city . ', ' . $state.', ' .$zip;
		$direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "";

	?>

<div class="storelocation" data-store="<?php echo get_the_title(); ?>">
	<div class="header-banner-image-bg">
		<div class="header-banner-image">
		</div>
	</div>
	
	<div class="header-sale-image">		
		
			<?php echo do_shortcode('[coupon "salebanner"]'); ?>	
		
	</div>
	<div class="container">
		<div class="row">	

				<div class="col-lg-5 col-md-6 col-sm-12 storeloc-content">
					<img src="/wp-content/uploads/2021/11/bobs-carpet-and-flooring.png"/>
					
					<h2 class="entry-title sfnstoretitle">Bob's Carpet & Flooring | <?php echo $city.', '.$state;  ?></h2>
					<!-- <span class="autorized">authorized BIG BOB'S DEALER</span>    -->
					
					<!-- <span class="siteUrl"><a href="https://<?php //echo $site_url;?>" target="_blank"><?php //echo $site_url;  ?></a></span>   -->

					<!-- <a href="https://<?php //echo $site_url;?>" target="_blank" class="fl-button" role="button">
							<span class="uabb-button-text uabb-creative-button-text">VISIT SITE</span></a> -->

					<!-- <a href="https://<?php //echo $site_url;?>/about-us/reviews/"><div class="store_reviews">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="number_review">SEE REVIEWS</span>
					</div></a> -->

					<span class="add"><a href="<?php echo $direction_url ;?>" target="_blank"><?php echo $destination;?></a></span>

					<div class="mapcon_right">
								<a href="<?php echo $direction_url ;?>" target="_blank">GET DIRECTIONS</a>
					</div>

					<?php if($phone!=''){?>
			
					<span class="phNo">PHONE NUMBER </span>
					<span class="phNoDis"><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></span>

					<?php } ?>
			
					<div class="storehours">
						<span>HOURS </span>
						<?php echo do_shortcode( '[wpsl_hours]' ); ?>
					</div>

					<div class="contact_btn">
								<a href="/contact-us/"  class="fl-button">CONTACT US</a>
					</div>
			
					<!-- <div class="store_links_main">
						<span class="getLable">BIG BOB'S SERVICES </span>
						<ul class="store_services">
							<li><a href="https://<?php //echo $site_url;?>/contact-us/" class="getdirect" target="_blank"> SHOP AT HOME</a></li>
							<li><a href="https://rugs.shop/en_us/?store=<?php //echo get_post_meta( $queried_object->ID, 'wpsl_rugshop_code', true ); ?>" class="getdirect" target="_blank"> BUY RUGS ONLINE</a></li>
							<li><a href="https://<?php// echo $site_url;?>/services/free-measurement/" class="button" target="_blank"> SCHEDULE A MEASURE</a></li>
						
						</ul>
					</div> -->
					
					<!-- <div class="store_links_main">
					<span class="getLable">PRODUCTS OFFERED</span>
						<ul class="product_offered">						
							<li><a href="https://<?php //echo $site_url;?>/flooring/carpet/products/" class="getdirect" target="_blank">CARPET</a></li>
							<li><a href="https://<?php //echo $site_url;?>/flooring/hardwood/products/" class="getdirect" target="_blank"> HARDWOOD</a></li>
							<li><a href="https://<?php //echo $site_url;?>/flooring/vinyl/products/" class="getdirect" target="_blank"> VINYL</a></li>
							<li><a href="https://<?php //echo $site_url;?>/flooring/laminate/products/" class="getdirect" target="_blank"> LAMINATE</a></li>
							<li><a href="https://<?php //echo $site_url;?>/flooring/tile/products/" class="getdirect" target="_blank" > TILE</a></li>
							<li><a href="https://rugs.shop/en_us/?store=<?php //echo get_post_meta( $queried_object->ID, 'wpsl_rugshop_code', true ); ?>" class="getdirect" target="_blank"> RUGS ONLINE</a></li>
						
						</ul>	
					</div> -->
					
				</div>

				
				<div class="col-lg-7 col-md-6 col-sm-12 storeloc-map">
					<div class="mapcontainer">
						<div class="mapcon_main">
							<div class="mapcon_left">
								<div class="mapcon_left_flex">
								<i class="ua-icon ua-icon-location-pin" aria-hidden="true"></i>
								<div>
									<h1 class="sfnstore_title">Bob's Carpet & Flooring in <?php echo $city.', '.$state;  ?></h1>
									<a href="<?php echo $direction_url ;?>"><span class="add"><?php echo $destination;?></span></a>
								</div>
								</div>							
							</div>
							<div class="mapcon_right">
								<a href="<?php echo $direction_url ;?>" target="_blank">GET DIRECTIONS</a>
							</div>
						</div>
						
						<div class="store_map_wrap">
														
							<?php echo do_shortcode( '[wpsl_map zoom="17"]' ); ?>

						</div>
										
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="bottomsection">
						<?php echo do_shortcode('[fl_builder_insert_layout slug="best-flooring"]'); ?>
					</div>
					
				</div>
			</div>
			<div class="row estimate_row">
				<div class="col-lg-12">
					<div class="bottomsection">
						<?php echo do_shortcode('[fl_builder_insert_layout slug="estimate-row-section"]'); ?>
					</div>
					
				</div>
			</div>
	</div>
</div>
<script>
 

 jQuery(window).on("load", function() {
    jQuery(".wpsl-gmap-canvas div[role=button] > img").trigger( "click" );
});

jQuery(document).ready(function() {

var mystore_loc =  jQuery(".storelocation").attr("data-store")
console.log(mystore_loc);
jQuery(".populate-store-location select").val(mystore_loc);

});
</script>

<?php get_footer(); ?>
